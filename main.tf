module "ec2_cluster" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "~> 2.0"

  name                   = "my-cluster"
  instance_count         = 1

  ami                    = "ami-05d72852800cbf29e"
  instance_type          = "t2.micro"
  key_name               = "kanishka_bansal"
  monitoring             = true
  vpc_security_group_ids = ["sg-0c48f74107917110d"]
  subnet_id              = "subnet-99af21d5"

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}